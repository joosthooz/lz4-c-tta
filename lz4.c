//#define DEBUG

#ifdef DEBUG
#include <stdio.h>
#undef DEBUG
#define DEBUG(...) fprintf(stderr, __VA_ARGS__)
#else
#define DEBUG(...)
#endif

#include "lz4.h"

int lz4_check_header_and_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {
  DEBUG("Trying to decompress framed LZ4\n");

  // Pop and check the magic number
  uint32_t magic = (pop()<<24) | (pop()<<16) | (pop()<<8) | pop();
  if (magic != 0x04224D18) { //LZ4
    DEBUG("Error; wrong magic number.\n");
    return 1;
  }

  return lz4_decomp(buf, pop, push);
}

int lz4_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t)) {
  DEBUG("Decompressing Framed LZ4\n");

  // Pop Frame descriptor and Block max size descriptor
  uint8_t flg = pop();
  uint8_t bd = pop();
  uint8_t max_block_size = ((bd >> 4) & 0x7);

  // Check maximum block size
  if (max_block_size != 4) {
    // we may only wish to support 64k block size for now.
    // However, the offset encoding is 16 bits, so 64k buffer for the window should be enough regardless.
    //return 1; 
    DEBUG("Warning: max block size = %d\n", max_block_size);
  }

  // Decode Frame descriptor
  uint8_t version_number = ((flg >> 6) & 0x3);
  DEBUG("version_number=%d\n", version_number);
  uint8_t block_independence = ((flg >> 5) & 0x1);
  DEBUG("block_independence=%d\n", block_independence);
  uint8_t block_checksum_present = ((flg >> 4) & 0x1);
  DEBUG("block_checksum_present=%d\n", block_checksum_present);
  uint8_t content_size_present = ((flg >> 3) & 0x1);
  DEBUG("content_size_present=%d\n", content_size_present);
  uint8_t content_checksum_present = ((flg >> 2) & 0x1);
  DEBUG("content_checksum_present=%d\n", content_checksum_present);
  if (version_number != 0x01) {
    return 1; //version must be set to 01
  }
  if (content_size_present) {
    uint32_t frame_size_l = pop() | (pop()<< 8) | (pop()<<16) | (pop()<<24);
    uint32_t frame_size_h = pop() | (pop()<< 8) | (pop()<<16) | (pop()<<24);
    DEBUG("frame_size_l=0x%x, frame_size_h=0x%x\n", frame_size_l, frame_size_h);
  }


  uint8_t frameheader_checksum = pop(); //don't bother checking it

  while (1) {
    uint32_t comp_size = pop() | (pop()<< 8) | (pop()<<16) | (pop()<<24);
    DEBUG("New block, size %d\n", comp_size);
    uint8_t is_compressed = (comp_size & (1 << 31)) == 0;
    comp_size = comp_size & 0x7fffffff;
    if (comp_size == 0) {
      // Decompressing finished
      DEBUG("Decompression finished.\n");
      if (content_checksum_present) {
        DEBUG("Discarding 4 bytes content checksum.\n");
        for (int i = 0; i < 4; i++) 
          pop(); //don't bother checking it
      }
      break;
    }

     //Could consider checking the block size

    if (is_compressed) {
      // The buf is a pointer sliding through the buffer, so keep it updated
      buf = lz4_decomp_block(buf, pop, push, comp_size, block_independence);
      if (block_checksum_present) {
        DEBUG("Discarding 4 bytes block checksum.\n");
        for (int i = 0; i < 4; i++) 
          pop(); //don't bother checking it
      }
    } else {
      DEBUG("uncompressed block, size=%d\n", comp_size);
      // Perform the copy.
      for (int i = 0; i < comp_size; i++) {
        push(pop());
      }
    }
  }
  return 0;
}

uint8_t *lz4_decomp_block(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t), 
                      uint32_t comp_size, uint8_t block_independence) {
  uint32_t   comp_pos = 0;
  uint32_t decomp_pos = 0;

  while (1) { // the last sequence stops right after the literals field, so I moved the loop condition check there
    DEBUG("comp_pos=%d, comp_size=%d\n", comp_pos, comp_size);

    int16_t token = pop();
    comp_pos++;

    // Handle Literal part.
    uint32_t lit_len = (token>>4) & 0xf;
    if (lit_len == 0xf) {
      uint8_t val = 0xff;
      do {
        val = pop();
        comp_pos++;
        lit_len += val;
      } while (val == 0xff);
    }
    DEBUG("lit len=%d\n", lit_len);

    // Update position.
// We're streaming so we can't check a buffer for overflowing
//        if (decomp_pos + lit_len > decomp_size) {
//          DEBUG("Error: literal extending beyond block size.");
//          return 4;
//        }
        decomp_pos += lit_len;

#define PERFORM_LITERAL()  { \
      uint8_t val = pop(); \
      comp_pos++; \
      *buf++ = val; \
      push(val); \
}
    // Copy the literal.
    uint8_t *endp = buf + lit_len;
    while (buf < endp) {
       PERFORM_LITERAL();
    }

    // The last sequence is only a literal, so we need to break out of the loop here
    if (comp_pos >= comp_size) break;

    // Handle Match part.
    // Decode offset.
    uint16_t copy_offs = pop() | (pop() << 8);
    comp_pos += 2;

    // Decode Match length
    uint32_t copy_len = token & 0xf;
    if (copy_len == 0xf) {
      uint8_t val = 0xff;
      do {
        val = pop();
        comp_pos++;
        copy_len += val;
      } while (val == 0xff);
    }
    copy_len += 4; // Add the base match length (MINMATCH)
    
    DEBUG("copy offs=0x%08X len=%d\n", copy_offs, copy_len);

    // Update position.
#ifndef __TCE__
    if (block_independence && (copy_offs > decomp_pos)) {
      // Without block independence, the match can refer back into the window
      DEBUG("Error: Match refering back beyond start of block.");
      return (uint8_t*)-1;
    }
#endif
// We're streaming so we can't check a buffer for overflowing
//        if (decomp_pos + copy_len > decomp_size) {
//          DEBUG("Error: Match extending beyond block size.");
//          return 4;
//        }
      decomp_pos += copy_len;

#define PERFORM_COPY()  { \
uint8_t val = *(buf - copy_offs); \
*buf++ = val; \
push(val); \
}
    // Perform the copy.
    uint8_t *endp_copy = buf + copy_len;
    while (buf < endp_copy) {
      PERFORM_COPY();
    }
  }
  return buf;
}


