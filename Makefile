ADF_FILE=6issue_fc_streamout.adf
OPT=-O3 -g

#run make X86=yesplease to compile for x86
ifndef X86
CFLAGS=-Wall -a ${ADF_FILE} -d -v ${TCE_STREAM} ${ADDRESS_SPACE}
LDFLAGS += -a ${ADF_FILE} -d -v
CC=tcecc
endif

ifndef TCE_STREAM_DISABLED
TCE_STREAM=-DTCE_STREAM
else
ifdef SEPARATE_ADDRESS_SPACE
ADDRESS_SPACE=-DSEPARATE_ADDRESS_SPACE
endif
endif


.PHONY: all
all: lz4 data.compressed
ifdef X86
	./lz4 < data.compressed > data.decompressed.out 
else
	cp data.compressed Streamin.in
	ttasim -a 6issue_fc_streamout.adf -e "prog lz4; run; info proc cycles; exit"
	cp Streamout.out data.decompressed.out
endif
	md5sum data.decompressed data.decompressed.out

lz4: main.c lz4.c
	${CC} $(OPT) ${CFLAGS} $^ -o lz4

data.compressed: data.decompressed
	lz4 -B4 -BD --no-frame-crc $< $@ #-B is block size, 4 is 64k. Default is 7, 256k. -BD is enable block dependency

data.decompressed:
	dd if=/dev/urandom of=$@.raw bs=1k count=100
	hexdump -C $@.raw > $@

.PHONY: clean
clean:
	rm -f data.compressed data.decompressed data.decompressed.out data.decompressed.raw lz4
