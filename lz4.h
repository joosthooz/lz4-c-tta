#ifndef _LZ4_H_
#define _LZ4_H_

#include <inttypes.h>

int lz4_decomp(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t));
uint8_t *lz4_decomp_block(uint8_t *buf, uint8_t (*pop)(void), void (*push)(uint8_t), 
                      uint32_t comp_size, uint8_t block_independence);


#endif
