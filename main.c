#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "lz4.h"

#ifdef TCE_STREAM
#include "tceops.h"
#endif

unsigned char *inpointer = (unsigned char*)0x800000;

#ifdef SEPARATE_ADDRESS_SPACE
__attribute__((address_space(1))) unsigned char *outpointer = (__attribute__((address_space(1))) unsigned char*)0x900000;
#else
unsigned char *outpointer = (unsigned char*)0x900000;
#endif

uint8_t input() {
#ifdef __TCE__
#ifndef TCE_STREAM
  unsigned char val = *inpointer;
  inpointer++;
#else //TCE_STREAM
  //Code for when you'd wish to first check if there is data availeble.
/*
  int status;
  while (1)
  {
    _TCE_STREAM_IN_STATUS(0, status);
    if (status == 1)
    break;
  }
*/
  uint8_t val;
  _TCE_STREAM_IN(0, val);
#endif //TCE_STREAM
#else //!__TCE__
  int16_t val;
  int err = read(0, &val, 1);
#endif//__TCE__
  return val;
}

void output(uint8_t val) {
#ifdef __TCE__
#ifndef TCE_STREAM
  *outpointer = val;
  outpointer++;
#else //TCE_STREAM
  _TCE_STREAM_OUT(val);
#endif
#else //!__TCE__
  write(1, &val, 1);
#endif
}

int main(int argc, char *argv[]) {
  uint8_t *buf = malloc(1024*1024);
  return lz4_check_header_and_decomp(buf, input, output);
}
